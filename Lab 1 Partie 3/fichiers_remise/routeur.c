/*
*********************************************************************************************************
*                                                 uC/OS-III
*                                          The Real-Time Kernel
*                                               PORT Windows
*
*
*            		          					Guy BOIS
*                                  Polytechnique Montreal, Qc, CANADA
*                                                Lab 1, Part 2 03/2021
*
*
*********************************************************************************************************
*/


#include "routeur.h"

#include "interrupts.h"


/*DECLARATION DES COMPTEURS POUR STATISTIQUES*/
int nbPacketCrees = 0;								// Nb de packets total créés
int nbPacketTraites = 0;							// Nb de paquets envoyés sur une interface
int nbPacketSourceRejete = 0;						// Nb de packets rejetés pour mauvaise source
int packet_rejete_fifo_pleine_inputQ = 0;			// Utilisation de la fifo d'entrée
int packet_rejete_fifo_pleine_Q = 0;				// Utilisation des 3 queues de priorité
int packet_rejete_output_port_plein = 0;			// Utilisation des MB
/**/
int delai_pour_vider_les_fifos_sec = 2;
int delai_pour_vider_les_fifos_msec = 0;
/**/
int routerIsOn = 0;

//#define safeprintf(fmt, ...)															\
//{																						\
//	OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &perr);						\
//	xil_printf(fmt, ##__VA_ARGS__);														\
//	OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &perr);									\
//}

// � utiliser pour ne pas avoir les traces de remplissage et de vidage des fifos
#define safeprintf(fmt, ...)															\
{																						\
}
/*
*********************************************************************************************************
*                                         WATCH DOG
*********************************************************************************************************
*/
CPU_STK          TaskPeriodicStk[TASK_STK_SIZE];     	// Startup    task stack

OS_TCB			 TaskPeriodicTCB;

OS_TMR      	Scheduler;

void			SchedulerFct(OS_TMR  *p_tmr, void    *p_arg);



/*
*********************************************************************************************************
*                                                  MAIN
*********************************************************************************************************
*/

int main (void)
{

 	OS_ERR  err;

    UCOS_LowLevelInit();

    CPU_Init();
    Mem_Init();
    OSInit(&err);

    create_application();

    OSStart(&err);
    return 0;                                         // Start multitasking
}

void create_application() {
	int error;

	error = create_events();
	if (error != 0)
		xil_printf("Error %d while creating events\n", error);

	error = create_tasks();
	if (error != 0)
		xil_printf("Error %d while creating tasks\n", error);

}

int create_tasks() {

	int i;

/*	for(i = 0; i < NB_OUTPUT_PORTS; i++)
	{
		Port[i].id = i;
		switch(i)
		{
			case 0:
				Port[i].name = "Port 0";
				break;
			case 1:
				Port[i].name = "Port 1";
				break;
			case 2:
				Port[i].name = "Port 2";
				break;
			default:
				break;
		};
	}
*/

	// Creation des taches
	OS_ERR err;

/*
	OSTaskCreate(&TaskGenerateTCB, 		"TaskGenerate", 	TaskGenerate,	(void*)0, 	TaskGeneratePRIO, 	&TaskGenerateSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskComputingTCB, 	"TaskComputing", 	TaskComputing, 	(void*)0, 	TaskComputingPRIO, 	&TaskComputingSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1024, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskForwardingTCB,	"TaskForwarding", 	TaskForwarding,	(void*)0, 	TaskForwardingPRIO, &TaskForwardingSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

// Pour éviter d'avoir 3 fois le même code on a un tableau pour lequel chaque entrée appel TaskOutputPort avec des paramètres différents
	for(i = 0; i < NB_OUTPUT_PORTS; i++){
	OSTaskCreate(&TaskOutputPortTCB[i], "OutputPort", 	TaskOutputPort, &Port[i], TaskOutputPortPRIO, &TaskOutputPortSTK[i][0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
	}; */


    OSTaskCreate(&StartupTaskTCB,"StartUp Task", StartupTask, (void *) 0, UCOS_START_TASK_PRIO,
                 &StartupTaskStk[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

    return 0;
}

int create_events() {
	OS_ERR err;
	int i;

	// Creation des semaphores
	// Pas de sémaphore pour la partie 1

	// Creation des mutex
	OSMutexCreate(&mutRejete, "mutRejete", &err);
	OSMutexCreate(&mutPrint, "mutPrint", &err);
	OSMutexCreate(&mutAlloc, "mutAlloc", &err);

	// Creation des files externes  - vous pourrez diminuer au besoin la longueur des files
	OSQCreate(&lowQ, "lowQ", 1024, &err);
	OSQCreate(&mediumQ, "mediumQ", 1024, &err);
	OSQCreate(&highQ, "highQ", 1024, &err);

	return 0;
}

/*
*********************************************************************************************************
*                                               STARTUP TASK
*********************************************************************************************************
*/

////DEFINITION
void fit_timer_10s_isr(void *p_int_arg, CPU_INT32U source_cpu){
	OS_ERR perr;
	CPU_TS ts;
	    // Traitement sur SemStats
		//OSTimeDlyHMSM(0, 0, 10, 0, OS_OPT_TIME_HMSM_STRICT, &perr);
	xil_printf("-STATS-10s-");
	OSSemPost(&SemStat,OS_OPT_POST_1 + OS_OPT_POST_NO_SCHED,&perr);
	//OSSemPost(&SemStat,OS_OPT_POST_1,&perr);
}


void fit_timer_3s_isr(void *p_int_arg, CPU_INT32U source_cpu){
	OS_ERR perr;
	// Traitement sur SemStop
	//OSTimeDlyHMSM(0, 0, 3, 0, OS_OPT_TIME_HMSM_STRICT, &perr);
	xil_printf("-TASKSTOP-");
	OSSemPost(&SemStop,OS_OPT_POST_1+ OS_OPT_POST_NO_SCHED,&perr);
	//OSSemPost(&SemStop,OS_OPT_POST_1,&perr);
}

void gpio_isr(void *p_int_arg, CPU_INT32U source_cpu){
CPU_TS ts;
OS_ERR err;
// Traitement sur SemReset
//À compléter
// On met à 0 les interruptions du GPIO avec l’aide du masque
//  XGPIO_IR_MASK
	OSSemPost(&SemReset,OS_OPT_POST_1 + OS_OPT_POST_NO_SCHED,&err);
	//OSSemPost(&SemReset,OS_OPT_POST_1,&err);
	xil_printf("--RESET--");
	routerIsOn=1;
	XGpio_InterruptClear(&gpSwitch, XGPIO_IR_MASK);
}


///////////////////////////////////////////////////////////////////////////////////////
//									TASKS
///////////////////////////////////////////////////////////////////////////////////////

/*
 *********************************************************************************************************
 *											  TaskGeneratePacket
 *  - Génère des paquets et les envoie dans la InputQ.
 *  - À des fins de développement de votre application, vous pouvez *temporairement* modifier la variable
 *    "shouldSlowthingsDown" à true pour ne générer que quelques paquets par seconde, et ainsi pouvoir
 *    déboguer le flot de vos paquets de manière plus saine d'esprit. Cependant, la correction sera effectuée
 *    avec cette variable à false.
 *********************************************************************************************************
 */
void TaskGenerate(void *data) {
	srand(42);
	OS_ERR err, perr;
	CPU_TS ts;
	bool isGenPhase = false; 		//Indique si on est dans la phase de generation ou non
	const bool shouldSlowThingsDown = false;		//Variable à modifier
	int packGenQty = (rand() % 250);
	while(true) {
		if (isGenPhase) {
			OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				Packet *packet = malloc(sizeof(Packet));
			OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);

			packet->src = rand() * (UINT32_MAX/RAND_MAX);
			packet->dst = rand() * (UINT32_MAX/RAND_MAX);
			packet->type = rand() % NB_PACKET_TYPE;

			for (int i = 0; i < ARRAY_SIZE(packet->data); ++i)
				packet->data[i] = (unsigned int)rand();
			packet->data[0] = nbPacketCrees;

			nbPacketCrees++;
//			OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//			//if (shouldSlowThingsDown) {
//				xil_printf("GENERATE : ********Generation du Paquet # %d ******** \n", nbPacketCrees);
//				xil_printf("ADD %x \n", packet);
//				xil_printf("	** src : %x \n", packet->src);
//				xil_printf("	** dst : %x \n", packet->dst);
//				xil_printf("	** type : %d \n", packet->type);
//			OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

			//}

			OSTaskQPost(&TaskComputingTCB, packet, sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);

			safeprintf("Nb de paquets dans inputQ - apres production de TaskGenenerate: %d \n", TaskComputingTCB.MsgQ.NbrEntries);

			if (err == OS_ERR_Q_MAX || err == OS_ERR_MSG_POOL_EMPTY) {

				OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				safeprintf("GENERATE: Paquet rejete a l'entree car la FIFO est pleine !\n");
					free(packet);
				OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
				packet_rejete_fifo_pleine_inputQ++;
			}

			if (shouldSlowThingsDown) {
				OSTimeDlyHMSM(0,0,0, 200 + rand() % 600, OS_OPT_TIME_HMSM_STRICT, &err);
			} else {
				OSTimeDlyHMSM(0, 0, 0, 1, OS_OPT_TIME_HMSM_STRICT, &err);

				if ((nbPacketCrees % packGenQty) == 0) //On genère au maximum 255 paquets par phase de géneration
				{
					isGenPhase = false;
				}
			}
		}
		else
		{
			OSTimeDlyHMSM(0, 0, delai_pour_vider_les_fifos_sec, delai_pour_vider_les_fifos_msec, OS_OPT_TIME_HMSM_STRICT, &err);
			isGenPhase = true;
			do { packGenQty = (rand() %255); } while (packGenQty == 0);

		//safeprintf("GENERATE: Generation de %d paquets durant les %d prochaines millisecondes\n", packGenQty, packGenQty*2);
		//	xil_printf("GENERATE: Generation de %d paquets durant les %d prochaines millisecondes\n", packGenQty, packGenQty*2);
		}
	}
}

/*
 *********************************************************************************************************
 *											  TaskStop
 *  -Stoppe le routeur une fois que 100 paquets ont été rejetés pour mauvais CRC
 *  -Ne doit pas stopper la tâche d'affichage des statistiques.
 *********************************************************************************************************
 */
// Partie 2 (oubliez ça pour l'instant)
void TaskStop(void *data){
	OS_ERR err;
	CPU_TS ts;
	while(true){
		OSSemPend(&SemStop, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		if (!routerIsOn) {
		    continue ;
		    }
		xil_printf("-----------------------Task stop check -----------------------\n");
		if (nbPacketSourceRejete >=5 ) {
			routerIsOn=0;
			TurnOnLED(COLOR_DOUBLE_YELLOW);
			OSTaskSuspend(&TaskGenerateTCB, &err);
			OSTaskSuspend(&TaskComputingTCB, &err);
			OSTaskSuspend(&TaskForwardingTCB, &err);
			for (int i = 0; i < NB_OUTPUT_PORTS; i++) {
				OSTaskSuspend(&TaskOutputPortTCB[i], &err);
			}
			OSTmrStart(&Scheduler, &err);
		}
	}
}


/*
 *********************************************************************************************************
 *											  TaskReset
 *  -Remet le compteur de mauvaises sources à 0
 *  -Si le routeur était arrêté, le redémarre
 *********************************************************************************************************
 */
void TaskReset(void *data){
	OS_ERR err;
	CPU_TS ts;
	while(true){
		OSSemPend(&SemReset, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
		nbPacketSourceRejete =0 ;
		xil_printf("----------------------- Task Reset -----------------------\n");
		routerIsOn=1;
		TurnOnLED(COLOR_DOUBLE_GREEN);
		OSStatReset(&err);
		if(err==OS_ERR_NONE){
			OSTaskResume(&TaskGenerateTCB,&err);
			OSTaskResume(&TaskComputingTCB,&err);
			OSTaskResume(&TaskForwardingTCB, &err);
			for(int i=0;i<NB_OUTPUT_PORTS;i++){
				OSTaskResume(&TaskOutputPortTCB[i],&err);
			}
		}
	}
}

/*
 *********************************************************************************************************
 *											  TaskComputing
 *  -Vérifie si les paquets sont conformes (CRC,Adresse Source)
 *  -Dispatche les paquets dans des files (HIGH,MEDIUM,LOW)
 *
 *********************************************************************************************************
 */
void TaskComputing(void *pdata) {
	OS_ERR err, perr;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;
	Packet *packet = NULL;
	OS_TICK actualticks = 0;
	while(true){

		packet = OSTaskQPend(0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);

		safeprintf("Nb de paquets dans inputQ - apres consommation de TaskComputing: %d \n", TaskComputingTCB.MsgQ.NbrEntries);

		err_msg("COMPUTING OSQPEND",err);

		/* On simule un temps de traitement avec ce compteur bidon.
		 * Cette boucle devrait prendre entre 2 et 4 ticks d'OS (considérez
		 * exactement 3 ticks pour la question dans l'énoncé).
		 */

		actualticks = OSTimeGet(&err);
		while (WAITFORComputing + actualticks > OSTimeGet(&err));

		//Verification de l'espace d'addressage
		if ((packet->src > REJECT_LOW1 && packet->src < REJECT_HIGH1) ||
			(packet->src > REJECT_LOW2 && packet->src < REJECT_HIGH2) ||
			(packet->src > REJECT_LOW3 && packet->src < REJECT_HIGH3) ||
			(packet->src > REJECT_LOW4 && packet->src < REJECT_HIGH4)) {
			OSMutexPend(&mutRejete, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				++nbPacketSourceRejete;

//			OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//			xil_printf("\n--TaskComputing: Source invalide (Paquet rejete) (total : %d)\n", nbPacketSourceRejete);
//			xil_printf("\n--Il s agit du paquet\n");
//			xil_printf("	** src : %x \n", packet->src);
//			xil_printf("	** dst : %x \n", packet->dst);
//			OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

			OSMutexPost(&mutRejete, OS_OPT_POST_NONE, &err);

			OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
					free(packet);
			OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
		} else {

			//Dispatche les paquets selon leur type
			switch (packet->type) {
			case PACKET_VIDEO:
				OSQPost(&highQ, packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
				safeprintf("Nb de paquets dans highQ - apres production de TaskComputing: %d \n", highQ.MsgQ.NbrEntries);
			break;

			case PACKET_AUDIO:
				OSQPost(&mediumQ, packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
				safeprintf("Nb de paquets dans mediumQ - apres production de TaskComputing: %d \n", mediumQ.MsgQ.NbrEntries);
			break;

			case PACKET_AUTRE:
				OSQPost(&lowQ, packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
				safeprintf("Nb de paquets dans lowQ - apres production de TaskComputing: %d \n", lowQ.MsgQ.NbrEntries);
			break;

			default:
				break;
			}
				if (err == OS_ERR_Q_MAX || err == OS_ERR_MSG_POOL_EMPTY) {
					OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
					safeprintf("TaskComputing: Paquet rejete car FIFO pleine\n");
					free(packet);
					OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
					packet_rejete_fifo_pleine_Q++;
				}
		}
	}
}


/*
 *********************************************************************************************************
 *											  TaskForwarding
 *  -traite la priorité des paquets : si un paquet de haute priorité est prêt,
 *   on l'envoie à l'aide de la fonction dispatch_packet, sinon on regarde les paquets de moins haute priorité
 *********************************************************************************************************
 */
void TaskForwarding(void *pdata) {
	OS_ERR err, perr;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;
	Packet *packet = NULL;

	while(1){
		/* Si paquet vidéo prêt */
		packet = OSQPend(&highQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
		safeprintf("Nb de paquets dans highQ - apres consommation de TaskFowarding: %d \n", highQ.MsgQ.NbrEntries);
		if(err == OS_ERR_NONE){
			/* Envoi du paquet */
			dispatch_packet(packet);
			safeprintf("\n--TaskForwarding:  paquets %d envoyes\n\n", ++nbPacketTraites);
		}else{
			/* Si paquet audio prêt */
			packet = OSQPend(&mediumQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
			safeprintf("Nb de paquets dans mediumQ - apres consommation de TaskFowarding: %d \n", mediumQ.MsgQ.NbrEntries);
			if(err == OS_ERR_NONE){
				/* Envoi du paquet */
				dispatch_packet(packet);
				safeprintf("\n--TaskForwarding: paquets %d envoyes\n\n", ++nbPacketTraites);
			}else{
				/* Si paquet autre prêt */
				packet = OSQPend(&lowQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
				safeprintf("Nb de paquets dans lowQ - apres consommation de TaskFowarding: %d \n", lowQ.MsgQ.NbrEntries);
				if(err == OS_ERR_NONE){
					/* Envoi du paquet */
					dispatch_packet(packet);
					safeprintf("\n--TaskForwarding: paquets %d envoyes\n\n", ++nbPacketTraites);
				}
			}
		}
	}
}

/*
 *********************************************************************************************************
 *											  Fonction Dispatch
 *  -Envoie le paquet passé en paramètre vers la mailbox correspondante à son adressage destination
 *********************************************************************************************************
 */
 void dispatch_packet (Packet* packet){
	OS_ERR err, perr;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;

	/* Test sur la destination du paquet */
	if(packet->dst >= INT1_LOW && packet->dst <= INT1_HIGH ){

		safeprintf("\n--Paquet dans mb1\n");
		OSTaskQPost(&TaskOutputPortTCB[0], packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
	}
	else {
			if (packet->dst >= INT2_LOW && packet->dst <= INT2_HIGH ){
			safeprintf("\n--Paquet dans mb2\n");
			OSTaskQPost(&TaskOutputPortTCB[1], packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
			}
			else {
				if(packet->dst >= INT3_LOW && packet->dst <= INT3_HIGH ){
					safeprintf("\n--Paquet dans mb3\n");
					OSTaskQPost(&TaskOutputPortTCB[2], packet, sizeof(packet),OS_OPT_POST_FIFO, &err);
					}
					else {
						if(packet->dst >= INT_BC_LOW && packet->dst <= INT_BC_HIGH){
						Packet* others[2];
						int i;
						for (i = 0; i < ARRAY_SIZE(others); ++i) {
							OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
							others[i] = malloc(sizeof(Packet));
							OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
							memcpy(others[i], packet, sizeof(Packet));
							}
						safeprintf("\n--Paquet BC dans mb1, mb2 et mb3\n");
						OSTaskQPost(&TaskOutputPortTCB[0], packet, sizeof(packet), OS_OPT_POST_FIFO, &err);
						OSTaskQPost(&TaskOutputPortTCB[1], others[0], sizeof(others[0]), OS_OPT_POST_FIFO, &err);
						OSTaskQPost(&TaskOutputPortTCB[2], others[1], sizeof(others[1]), OS_OPT_POST_FIFO, &err);
						}
					}
			}
	}
		if(err == OS_ERR_Q_MAX || err == OS_ERR_MSG_POOL_EMPTY) {
			/*Destruction du paquet si la mailbox de destination est pleine*/

			OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				safeprintf("\n--TaskForwarding: Erreur mailbox full\n");
				free(packet);
				packet_rejete_output_port_plein++;
			OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);

		}
 }

/*
 *********************************************************************************************************
 *											  TaskPrint
 *  -Affiche les infos des paquets arrivés à destination et libere la mémoire allouée
 *********************************************************************************************************
 */
void TaskOutputPort(void *data) {
	OS_ERR err, perr;
	CPU_TS ts;
	OS_MSG_SIZE msg_size;
	Packet* packet = NULL;
	Info_Port info = *(Info_Port*)data;

	while(1){
		/*Attente d'un paquet*/
		packet = OSTaskQPend (0, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);
		err_msg("PRINT : MBoxPend",err);

//		OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
//		/*impression des infos du paquets*/
//		xil_printf("\nPaquet recu en %d \n", info.id);
//		xil_printf("    >> src : %x \n", packet->src);
//		xil_printf("    >> dst : %x \n", packet->dst);
//		xil_printf("    >> type : %d \n", packet->type);
//		OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

		/*Libération de la mémoire*/
		OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			free(packet);
		OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
	}

}

/*
 *********************************************************************************************************
 *                                              TaskStats
 *  -Est déclenchée lorsque le gpio_isr() libère le sémaphore
 *  -Lorsque déclenchée, imprime les statistiques du routeur à cet instant
 *********************************************************************************************************
 */
 void TaskStats(void* pdata) {
	OS_ERR err, perr;
	CPU_TS ts;
	OS_TICK actualticks;

//	Suspend_Delay_Resume_All(1);
	xil_printf("Stats");

	while (1) {
		OSSemPend(&SemStat,0,OS_OPT_PEND_BLOCKING, &ts,&err);


		OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);

		xil_printf("\n------------------ Affichage des statistiques ------------------\n\n");
		xil_printf("Delai pour vider les fifos sec: %d\n", delai_pour_vider_les_fifos_sec);
		xil_printf("Delai pour vider les fifos msec: %d\n", delai_pour_vider_les_fifos_msec);
		xil_printf("Frequence du systeme: %d\n", OS_CFG_TICK_RATE_HZ);
		xil_printf("1 - Nb de packets total crees : %d\n", nbPacketCrees);
		xil_printf("2 - Nb de packets total traites : %d\n", nbPacketTraites);
		xil_printf("3 - Nb de packets rejetes pour mauvaise source : %d\n", nbPacketSourceRejete);
		xil_printf("4 - Nb de paquets rejetes dans fifo d'entree: %d\n", packet_rejete_fifo_pleine_inputQ);
		xil_printf("4 - Nb de paquets rejetes dans 3 Q: %d\n", packet_rejete_fifo_pleine_Q);
		xil_printf("5 - Nb de paquets rejetes dans l'interface de sortie: %d\n\n", packet_rejete_output_port_plein);
		xil_printf("6 - Nb de paquets maximum dans le fifo d'entree : %d \n", TaskComputingTCB.MsgQ.NbrEntriesMax);
		xil_printf("7 - Nb de paquets maximum dans highQ : %d \n", highQ.MsgQ.NbrEntriesMax);
		xil_printf("8 - Nb de paquets maximum dans mediumQ : %d \n", mediumQ.MsgQ.NbrEntriesMax);
		xil_printf("9 - Nb de paquets maximum dans lowQ : %d \n\n", lowQ.MsgQ.NbrEntriesMax);
		xil_printf("10 - Pourcentage de temps CPU Max de TaskGenerate: %d \n", TaskGenerateTCB.CPUUsageMax/100);
		xil_printf("10 - Pourcentage de temps CPU  de TaskGenerate: %d \n", TaskGenerateTCB.CPUUsage / 100);
		xil_printf("11 - Pourcentage de temps CPU Max de TaskComputing: %d \n", TaskComputingTCB.CPUUsageMax/100);
		xil_printf("12 - Pourcentage de temps CPU Max de TaskForwarding: %d \n", TaskForwardingTCB.CPUUsageMax/100);
		xil_printf("13 - Pourcentage de temps CPU Max de TaskOutputPort no 1: %d \n", TaskOutputPortTCB[0].CPUUsageMax/100);
		xil_printf("14 - Pourcentage de temps CPU Max de TaskOutputPort no 2: %d \n", TaskOutputPortTCB[1].CPUUsageMax/100);
		xil_printf("15 - Pourcentage de temps CPU Max de TaskOutputPort no 3: %d \n", TaskOutputPortTCB[2].CPUUsageMax/100);
		xil_printf("16- Pourcentage de temps CPU  : %d \n", OSStatTaskCPUUsage / 100);
		xil_printf("17- Pourcentage de temps CPU Max : %d \n", OSStatTaskCPUUsageMax / 100);
		xil_printf("18- Message free : %d \n", OSMsgPool.NbrFree);
		xil_printf("19- Message used : %d \n", OSMsgPool.NbrUsed);
		xil_printf("20- Message used max : %d \n", OSMsgPool.NbrUsedMax);

		OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

//		Suspend_Delay_Resume_All(10);

		//OSTimeDlyHMSM(0, 0, 1, 0, OS_OPT_TIME_HMSM_STRICT, &err);
	}
}

/*
 *********************************************************************************************************
 *                                              Suspend_Delay_Resume
 *  -Utilise lors de l'initialisation de la tache statistique
 *  -Permet aussi d'arrêter l'exécution durant l'exécution
 *********************************************************************************************************
 */

void Suspend_Delay_Resume_All(int nb_sec) {

OS_ERR err;
int i;

	OSTaskSuspend(&TaskGenerateTCB, &err);
	OSTaskSuspend(&TaskComputingTCB, &err);
	OSTaskSuspend(&TaskForwardingTCB, &err);


	for (i = 0; i < NB_OUTPUT_PORTS; i++) {
		OSTaskSuspend(&TaskOutputPortTCB[i], &err);
	};


	OSTimeDlyHMSM(0, 0, nb_sec, 0, OS_OPT_TIME_HMSM_STRICT, &err);

	OSTaskResume(&TaskGenerateTCB, &err);
	OSTaskResume(&TaskComputingTCB, &err);
	OSTaskResume(&TaskForwardingTCB, &err);

	OSTaskResume(&TaskOutputPortTCB[i], &err);

}


void err_msg(char* entete, uint8_t err)
{
	if(err != 0)
	{
		xil_printf(entete);
		xil_printf(": Une erreur est retournée : code %d \n",err);
	}
}



void StartupTask (void *p_arg)
{
	int i;
	CPU_TS ts;
	OS_ERR err;
    KAL_ERR kal_err;
    CPU_INT32U tick_rate;
#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    MEM_SEG_INFO seg_info;
    LIB_ERR lib_err;
#endif
#if (APP_OSIII_ENABLED == DEF_ENABLED)
#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
    OS_ERR  os_err;
#endif
#endif


    UCOS_IntInit();
    initialize_axi_intc();
	initialize_gpio();
	connect_axi();


#if (APP_OSIII_ENABLED == DEF_ENABLED)
    tick_rate = OS_CFG_TICK_RATE_HZ;
#endif

#if (APP_OSII_ENABLED == DEF_ENABLED)
    tick_rate = OS_TICKS_PER_SEC;
#endif

    UCOS_TmrTickInit(tick_rate);                                /* Configure and enable OS tick interrupt.              */

#if (APP_OSIII_ENABLED == DEF_ENABLED)
#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
    OSStatTaskCPUUsageInit(&os_err);
#endif
#endif

    KAL_Init(DEF_NULL, &kal_err);
    UCOS_StdInOutInit();
    UCOS_PrintfInit();


#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    UCOS_Print("UCOS - uC/OS Init Started.\r\n");
    UCOS_Print("UCOS - STDIN/STDOUT Device Initialized.\r\n");
#endif

#if (APP_SHELL_ENABLED == DEF_ENABLED)
    UCOS_Shell_Init();
#endif

#if ((APP_FS_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_FS == DEF_ENABLED))
    UCOS_FS_Init();
#endif

#if ((APP_TCPIP_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_NET == DEF_ENABLED))
    UCOS_TCPIP_Init();
#endif /* (APP_TCPIP_ENABLED == DEF_ENABLED) */

#if ((APP_USBD_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_USBD == DEF_ENABLED) && (UCOS_USB_TYPE == UCOS_USB_TYPE_DEVICE))
    UCOS_USBD_Init();
#endif /* #if (APP_USBD_ENABLED == DEF_ENABLED) */

#if ((APP_USBH_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_USBH == DEF_ENABLED) && (UCOS_USB_TYPE == UCOS_USB_TYPE_HOST))
    UCOS_USBH_Init();
#endif /* #if (APP_USBH_ENABLED == DEF_ENABLED) */

#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    Mem_SegRemSizeGet(DEF_NULL, 4, &seg_info, &lib_err);

    UCOS_Printf ("UCOS - UCOS init done\r\n");
    UCOS_Printf ("UCOS - Total configured heap size. %d\r\n", seg_info.TotalSize);
    UCOS_Printf ("UCOS - Total used size after init. %d\r\n", seg_info.UsedSize);
#endif

    UCOS_Print("Programme initialise - \r\n");


	OSStatReset(&err);

	// On crée les tâches

	for(i = 0; i < NB_OUTPUT_PORTS; i++)
	{
		Port[i].id = i;
		switch(i)
		{
			case 0:
				Port[i].name = "Port 0";
				break;
			case 1:
				Port[i].name = "Port 1";
				break;
			case 2:
				Port[i].name = "Port 2";
				break;
			default:
				break;
		};
	}
	OSSemCreate(&SemStat, "SemStat", 0, &err);
	OSSemCreate(&SemReset, "SemReset", 0, &err);
	OSSemCreate(&SemStop, "SemStop", 0, &err);


	OSTaskCreate(&TaskGenerateTCB, 		"TaskGenerate", 	TaskGenerate,	(void*)0, 	TaskGeneratePRIO, 	&TaskGenerateSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskComputingTCB, 	"TaskComputing", 	TaskComputing, 	(void*)0, 	TaskComputingPRIO, 	&TaskComputingSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1024, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskForwardingTCB,	"TaskForwarding", 	TaskForwarding,	(void*)0, 	TaskForwardingPRIO, &TaskForwardingSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskStopTCB,	"TaskStop", 	TaskStop,	(void*)0, 	TaskStopPRIO, &TaskStopSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskResetTCB,	"TaskReset", 	TaskReset,	(void*)0, 	TaskResetPRIO, &TaskResetSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );


// Pour éviter d'avoir 3 fois le même code on a un tableau pour lequel chaque entrée appel TaskOutputPort avec des paramètres différents
	for(i = 0; i < NB_OUTPUT_PORTS; i++){
		OSTaskCreate(&TaskOutputPortTCB[i], "OutputPort", 	TaskOutputPort, &Port[i], TaskOutputPortPRIO, &TaskOutputPortSTK[i][0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
	};

	OSTaskCreate(&TaskStatsTCB, "TaskStats", TaskStats, (void*)0, TaskStatsPRIO, &TaskStatsSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

	OSTaskSuspend(&TaskGenerateTCB, &err);
	OSTaskSuspend(&TaskComputingTCB, &err);
	OSTaskSuspend(&TaskForwardingTCB, &err);
	for (int i = 0; i < NB_OUTPUT_PORTS; i++) {
		OSTaskSuspend(&TaskOutputPortTCB[i], &err);
	}

	TurnOnLED(COLOR_DOUBLE_PURPLE);
	OSSemCreate(&Synchro, "Synchro", 0, &err);
	OSTmrCreate(&Scheduler,         		/* p_tmr          */
		                   "Watch Dog",           	/* p_name         */
		                    0,                    	/* dly            */
		                    20,                   	/* period         */
		                    OS_OPT_TMR_PERIODIC,   	/* opt            */
							SchedulerFct,         	/* p_callback     */
		                    0,                     	/* p_callback_arg */
		                   &err);                  	/* p_err          */

	OSTaskSuspend((OS_TCB *)0,&err);

	OSSemPend(&Synchro,0, OS_OPT_PEND_BLOCKING, &ts, &err);
	OSTaskDel(&TaskGenerateTCB,&err);
	OSTaskDel(&TaskComputingTCB,&err);
	OSTaskDel(&TaskForwardingTCB,&err);
	OSTaskDel(&TaskStopTCB,&err);
	OSTaskDel(&TaskResetTCB,&err);
	OSTaskDel(&TaskStatsTCB,&err);
	for (int i = 0; i < NB_OUTPUT_PORTS; i++) {
			OSTaskSuspend(&TaskOutputPortTCB[i], &err);
		}
	OSSEmDel(&SemStat,&err);
	OSSEmDel(&SemStop,&err);
	OSSEmDel(&SemReset,&err);
	void cleanup();


}

void  SchedulerFct (OS_TMR  *p_tmr,
                     void    *p_arg)
{
	OS_ERR  err;
	TurnOnLED(COLOR_DOUBLE_RED);
	OSSemPost(&Synchro, OS_OPT_POST_1, &err);
}

