connect -url tcp:127.0.0.1:3121
source C:/Users/MAIN/Vivado/INF3610/cora_test/cora_test.sdk/design_1_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AD791AA"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AD791AA" && level==0} -index 1
fpga -file C:/Users/MAIN/Vivado/INF3610/cora_test/cora_test.sdk/design_1_wrapper_hw_platform_0/design_1_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AD791AA"} -index 0
loadhw -hw C:/Users/MAIN/Vivado/INF3610/cora_test/cora_test.sdk/design_1_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AD791AA"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AD791AA"} -index 0
dow C:/Users/MAIN/Vivado/INF3610/cora_test/cora_test.sdk/lab1_partie3/Debug/lab1_partie3.elf
configparams force-mem-access 0
