/*
*********************************************************************************************************
*                                                 uC/OS-III
*                                          The Real-Time Kernel
*                                               PORT Windows
*
*
*            		          					Guy BOIS
*                                  Polytechnique Montreal, Qc, CANADA
*                                                  02/2021
*									
*
*
*						Modifié par Paul Clas 1846912 & Sébastien Zerbato 1573817
*
*
*********************************************************************************************************
*/


#include "routeur.h"

#include  <cpu.h>
#include  <lib_mem.h>

#include <os.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

#include <app_cfg.h>
#include <cpu.h>
#include <ucos_bsp.h>
#include <ucos_int.h>
#include <xparameters.h>
#include <KAL/kal.h>

#include <xil_printf.h>

#include  <stdio.h>
#include  <ucos_bsp.h>

#include <Source/os.h>
#include <os_cfg_app.h>

#include <xgpio.h>
#include <xintc.h>
#include <xil_exception.h>

// � utiliser pour suivre le remplissage et le vidage des fifos
// Mettre en commentaire et utiliser la fonction vide suivante si vous ne voulez pas de trace
#define safeprintf(fmt, ...)															\
{																						\
	OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &perr);						\
	xil_printf(fmt, ##__VA_ARGS__);															\
	OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &perr);									\
}

// � utiliser pour ne pas avoir les traces de remplissage et de vidage des fifos
//#define safeprintf(fmt, ...)															\
//{																						\
//}

XIntc axi_intc;
XGpio gpSwitch;

#define GIC_DEVICE_ID	        XPAR_PS7_SCUGIC_0_DEVICE_ID
#define PL_INTC_IRQ_ID          XPS_IRQ_INT_ID
#define FIT_1S_IRQ_ID           XPAR_AXI_INTC_0_FIT_TIMER_0_INTERRUPT_INTR
#define FIT_3S_IRQ_ID			XPAR_AXI_INTC_0_FIT_TIMER_1_INTERRUPT_INTR
#define GPIO_SW_IRQ_ID			XPAR_AXI_INTC_0_AXI_GPIO_0_IP2INTC_IRPT_INTR
#define GPIO_SW_DEVICE_ID		XPAR_AXI_GPIO_0_DEVICE_ID

#define XGPIO_IR_MASK		0x3 /**< Mask of all bits */


/*
*********************************************************************************************************
*                                                  MAIN
*********************************************************************************************************
*/

int main (void)
{

 	OS_ERR  err;

    UCOS_LowLevelInit();

    CPU_Init();
    Mem_Init();
    OSInit(&err);

    create_application();

    OSStart(&err);
    return 0;                                         // Start multitasking
}

void create_application() {
	int error;

	error = create_events();
	if (error != 0)
		xil_printf("Error %d while creating events\n", error);

	error = create_tasks();
	if (error != 0)
		xil_printf("Error %d while creating tasks\n", error);

}

int create_tasks() {

	int i;
	OS_ERR err;

/* On fait diff�remment de sur VS en ce sens qu'ici on va cr�er seulement TaskStatsTCB et StartupTaskTCB,
   puis on va d�marrerer les taches applicatives *i.e. concernant le routeur) � la fin de la t�che Startup
   */


    /*OSTaskCreate(&StartupTaskTCB,"StartUp Task", StartupTask, (void *) 0, UCOS_START_TASK_PRIO,
                 &StartupTaskStk[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);*/
	for (i = 0; i < NB_OUTPUT_PORTS; i++) {
			Port[i].id = i;
			switch (i) {
			case 0:
				Port[i].name = "Port 0";
				break;
			case 1:
				Port[i].name = "Port 1";
				break;
			case 2:
				Port[i].name = "Port 2";
				break;
			default:
				break;
			};
		}

		// Creation des taches
		OS_ERR err;
		OSTaskCreate(&TaskGenerateTCB, "TaskGenerate", TaskGenerate, (void*)0, TaskGeneratePRIO, &TaskGenerateSTK[0u],
			TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		// Création de tâches à compléter
		OSTaskCreate(&TaskComputingTCB, "TaskComputing", TaskComputing, (void*)0, TaskComputingPRIO, &TaskComputingSTK[0u],
			TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		OSTaskCreate(&TaskForwardingTCB, "TaskForwarding", TaskForwarding, (void*)0, TaskForwardingPRIO, &TaskForwardingSTK[0u],
			TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		for (i = 0; i < NB_OUTPUT_PORTS; i++) {
			OSTaskCreate(&TaskOutputPortTCB[i], "TaskOutputPort", TaskOutputPort, &Port[i], TaskOutputPortPRIO,
				&TaskOutputPortSTK[i][0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 20, 0, (void*)0,
				(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
		};

		OSTaskCreate(&TaskStatsTCB, "TaskStats", TaskStats, (void*)0, TaskStatsPRIO, &TaskStatsSTK[0u],
			TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		OSTaskCreate(&StartupTaskTCB,"Main Task", StartupTask, (void *) 0, UCOS_START_TASK_PRIO, &StartupTaskStk[0u],
				0, UCOS_START_TASK_STACK_SIZE, 0, 0, DEF_NULL, (OS_OPT_TASK_STK_CHK |OS_OPT_TASK_STK_CLR), &err);


		OSTaskCreate(&TaskStopTCB, "TaskStop", TaskStop, (void*) 0, TaskStopPRIO, &TaskStopSTK[0u],
			    TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		OSTaskCreate(&TaskStopTCB, "TaskStop", TaskStop, (void*) 0, TaskStopPRIO, &TaskStopSTK[0u],
			    TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

		OSTaskCreate(&TaskResetTCB, "TaskReset", TaskReset, (void*) 0, TaskResetPRIO, &TaskResetSTK[0u],
			    TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);

    return 0;
}

int create_events() {
	OS_ERR err;
	int i;

	// Creation des semaphores
	// Pas de s�maphore pour la partie 1

	// Creation des mutex
	OSMutexCreate(&mutRejete, "mutRejete", &err);
	OSMutexCreate(&mutPrint, "mutPrint", &err);
	OSMutexCreate(&mutAlloc, "mutAlloc", &err);

	// Creation des files externes  - vous pourrez diminuer au besoin la longueur des files
	OSQCreate(&inputQ, "inputQ", 1024, &err);
	OSQCreate(&lowQ, "lowQ", 1024, &err);
	OSQCreate(&mediumQ, "mediumQ", 1024, &err);
	OSQCreate(&highQ, "highQ", 1024, &err);

	for (i = 0; i < NB_OUTPUT_PORTS; i++) {
			OSQCreate(&MBPort[i], "MailBox", 1, &err);
		}
	return 0;
}

/*
*********************************************************************************************************
*                                               STARTUP TASK
*********************************************************************************************************
*/


///////////////////////////////////////////////////////////////////////////////////////
//									TASKS
///////////////////////////////////////////////////////////////////////////////////////

/*
 *********************************************************************************************************
 *											  TaskGeneratePacket
 *  - G�n�re des paquets et les envoie dans la InputQ.
 *  - � des fins de d�veloppement de votre application, vous pouvez *temporairement* modifier la variable
 *    "shouldSlowthingsDown" � true pour ne g�n�rer que quelques paquets par seconde, et ainsi pouvoir
 *    d�boguer le flot de vos paquets de mani�re plus saine d'esprit. Cependant, la correction sera effectu�e
 *    avec cette variable � false.
 *********************************************************************************************************
 */
void TaskGenerate(void *data) {
	srand(42);
	OS_ERR err, perr;
	CPU_TS ts;
	bool isGenPhase = false; 		//Indique si on est dans la phase de generation ou non
	const bool shouldSlowThingsDown = false;		//Variable � modifier
	int packGenQty = (rand() % 250);
	while(true) {
		if (isGenPhase) {
			OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				Packet *packet = malloc(sizeof(Packet));
			OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);

			packet->src = rand() * (UINT32_MAX/RAND_MAX);
			packet->dst = rand() * (UINT32_MAX/RAND_MAX);
			packet->type = rand() % NB_PACKET_TYPE;

			for (int i = 0; i < ARRAY_SIZE(packet->data); ++i)
				packet->data[i] = (unsigned int)rand();
			packet->data[0] = nbPacketCrees;

			nbPacketCrees++;
			OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
			//if (shouldSlowThingsDown) {
				xil_printf("GENERATE : ********Generation du Paquet # %d ******** \n", nbPacketCrees);
				xil_printf("ADD %x \n", packet);
				xil_printf("	** src : %x \n", packet->src);
				xil_printf("	** dst : %x \n", packet->dst);
				xil_printf("	** type : %d \n", packet->type);
			OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

			//}

			OSTaskQPost(&TaskComputingTCB, packet, sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);

			safeprintf("Nb de paquets dans inputQ - apres production de TaskGenenerate: %d \n", TaskComputingTCB.MsgQ.NbrEntries);

			if (err == OS_ERR_Q_MAX) {

				OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				safeprintf("GENERATE: Paquet rejete a l'entree car la FIFO est pleine !\n");
					free(packet);
				OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
				packet_rejete_fifo_pleine_inputQ++;
			}

			if (shouldSlowThingsDown) {
				OSTimeDlyHMSM(0,0,0, 200 + rand() % 600, OS_OPT_TIME_HMSM_STRICT, &err);
			} else {
				OSTimeDlyHMSM(0, 0, 0, 1, OS_OPT_TIME_HMSM_STRICT, &err);

				if ((nbPacketCrees % packGenQty) == 0) //On gen�re au maximum 255 paquets par phase de g�neration
				{
					isGenPhase = false;
				}
			}
		}
		else
		{
			OSTimeDlyHMSM(0, 0, 2, 0, OS_OPT_TIME_HMSM_STRICT, &err);
			isGenPhase = true;
			do { packGenQty = (rand() %255); } while (packGenQty == 0);

			safeprintf("GENERATE: Generation de %d paquets durant les %d prochaines millisecondes\n", packGenQty, packGenQty*2);
		}
	}
}

/*
 *********************************************************************************************************
 *											  TaskStop
 *  -Stoppe le routeur une fois que 100 paquets ont �t� rejet�s pour mauvais CRC
 *  -Ne doit pas stopper la t�che d'affichage des statistiques.
 *********************************************************************************************************
 */
// Partie 2 (oubliez �a pour l'instant)



/*
 *********************************************************************************************************
 *											  TaskReset
 *  -Remet le compteur de mauvaises sources � 0
 *  -Si le routeur �tait arr�t�, le red�marre
 *********************************************************************************************************
 */
// Partie 2 (oubliez �a pour l'instant)

/*
 *********************************************************************************************************
 *											  TaskComputing
 *  -V�rifie si les paquets sont conformes (CRC,Adresse Source)
 *  -Dispatche les paquets dans des files (HIGH,MEDIUM,LOW)
 *
 *********************************************************************************************************
 */
void TaskComputing(void *pdata) {
		OS_ERR err;
		Packet *packet = NULL;
		bool validPacket;
		uint32_t compteur;
		while(true){
			packet = OSQPend(inputQ, 0, &err);
			validPacket = true;
			compteur = 0;
			err_msg("Task Computing error", err);

			if ((packet->src >= REJECT_LOW1 && packet->src <= REJECT_HIGH1) ||
				(packet->src >= REJECT_LOW2 && packet->src <= REJECT_HIGH2) ||
				(packet->src >= REJECT_LOW3 && packet->src <= REJECT_HIGH3) ||
				(packet->src >= REJECT_LOW4 && packet->src <= REJECT_HIGH4))
			{
				OSMutexPend(mutAlloc, 0, &err);
				free(packet);
				OSMutexPost(mutAlloc);
				validPacket = false;
				OSMutexPend(mutexRejete, 0, &err);
				++nbPacketSourceRejete;
				OSMutexPost(mutexRejete);
			}

			if (validPacket)
			{
				switch (packet->type)
				{
					case PACKET_VIDEO:
						err = OSQPost(highQ, packet);
						break;
					case PACKET_AUDIO:
						err = OSQPost(mediumQ, packet);
						break;
					case PACKET_AUTRE:
						err = OSQPost(lowQ, packet);
						break;
					default:
						OSMutexPend(mutAlloc, 0, &err);
						free(packet); //Discard le paquet car son type n'est pas défini.
						OSMutexPost(mutAlloc);
						break;
				}
				if (err != OS_NO_ERR)
				{
					err_msg("Erreur en placant un paquet dans une queue", err);
					OSMutexPend(mutAlloc, 0, &err);
					free(packet);
					OSMutexPost(mutAlloc);
				}
			}
			nbPacketTraites++;

			while(compteur++ <= 220000){}

		}

}


/*
 *********************************************************************************************************
 *											  TaskForwarding
 *  -traite la priorit� des paquets : si un paquet de haute priorit� est pr�t,
 *   on l'envoie � l'aide de la fonction dispatch_packet, sinon on regarde les paquets de moins haute priorit�
 *********************************************************************************************************
 */
void TaskForwarding(void *pdata) {
	OS_ERR err;
		CPU_TS ts;
		OS_MSG_SIZE msg_size;
		Packet* packet = NULL;

		while (1) {
			packet = OSQPend(&highQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
			if (err == OS_ERR_NONE) {
				dispatch_packet(packet);
			}
			else {
				packet = OSQPend(&mediumQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
				if (err == OS_ERR_NONE) {
					dispatch_packet(packet);
				}
				else {
					packet = OSQPend(&lowQ, 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
					if (err == OS_ERR_NONE) {
						dispatch_packet(packet);
					}
				}
			}
		}
}

/*
 *********************************************************************************************************
 *											  Fonction Dispatch
 *  -Envoie le paquet pass� en param�tre vers la mailbox correspondante � son adressage destination
 *********************************************************************************************************
 */
 void dispatch_packet (Packet* packet){
	 OS_ERR err;
	 	CPU_TS ts;
	 	OS_MSG_SIZE msg_size;

	 	bool interface1 = ((packet->dst >= INT1_LOW) && (packet->dst < INT1_HIGH));
	 	bool interface2 = ((packet->dst >= INT2_LOW) && (packet->dst < INT2_HIGH));
	 	bool interface3 = ((packet->dst >= INT3_LOW) && (packet->dst < INT3_HIGH));
	 	bool broadcast = ((packet->dst >= INT_BC_LOW) && (packet->dst < INT_BC_HIGH));


	 	if (broadcast) {
	 		OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
	 		Packet* packet2 = malloc(sizeof(Packet));
	 		Packet* packet3 = malloc(sizeof(Packet));
	 		OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
	 		*packet2 = *packet3 = *packet;
	 		Packet* packets[NB_OUTPUT_PORTS] = { packet, packet2, packet3 };
	 		for (int i = 0; i < NB_OUTPUT_PORTS; i++) {
	 			OSQPost(&MBPort[i], packets[i], sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);
	 		}
	 	}
	 	else if (interface1) {
	 		OSQPost(&MBPort[0], packet, sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);
	 	}
	 	else if (interface2) {
	 		OSQPost(&MBPort[1], packet, sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);
	 	}
	 	else if (interface3) {
	 		OSQPost(&MBPort[2], packet, sizeof(packet), OS_OPT_POST_FIFO + OS_OPT_POST_NO_SCHED, &err);
	 	}
 }

/*
 *********************************************************************************************************
 *											  TaskPrint
 *  -Affiche les infos des paquets arriv�s � destination et libere la m�moire allou�e
 *********************************************************************************************************
 */
void TaskOutputPort(void *data) {
	OS_ERR err;
		CPU_TS ts;
		OS_MSG_SIZE msg_size;
		Packet* packet = NULL;
		Info_Port info = *(Info_Port*)data;
		while (1) {
			packet = OSQPend(&MBPort[info.id], 0, OS_OPT_PEND_NON_BLOCKING, &msg_size, &ts, &err);
			if (packet != NULL) {
				OSMutexPend(&mutAlloc, 0, OS_OPT_PEND_BLOCKING, &ts, &err);
				safeprintf("Paquet recu en %d\n>>src: %x\n>>dst: %x\n>>type: %d\n\n",
							info.id,packet->dst, packet->dst, packet->type);
				free(packet);
				OSMutexPost(&mutAlloc, OS_OPT_POST_NONE, &err);
			}

			OSTimeDly(1, OS_OPT_TIME_DLY, &err);
		}
}

/*
 *********************************************************************************************************
 *                                              TaskStats
 *  -Est d�clench�e lorsque le gpio_isr() lib�re le s�maphore
 *  -Lorsque d�clench�e, imprime les statistiques du routeur � cet instant
 *********************************************************************************************************
 */
 void TaskStats(void* pdata) {
	 OS_ERR err, perr;
	 CPU_TS ts;
	 	OS_TICK actualticks;

	 	OSStatTaskCPUUsageInit(&err);
	 	Suspend_Delay_Resume_All(1);
	 	OSStatReset(&err);

	 	while (1) {


	 		OSMutexPend(&mutPrint, 0, OS_OPT_PEND_BLOCKING, &ts, &err);

	 		xil_printf("\n------------------ Affichage des statistiques ------------------\n\n");

	 		//		À compléter en utilisant la numérotation de 1 à 15  dans l'énoncé du laboratoire
	 		xil_printf("1 : Nb de paquets total créés: %d\n", nbPacketCrees);
	 		xil_printf("2: Nb de paquets total traités: %d\n", nbPacketTraites);
	 		xil_printf("3: Nb de paquets rejetés pour mauvaise source (adresse)  %d\n", nbPacketSourceRejete);
	 		xil_printf("4: Nb de paquets rejetés dans la fifo d’entrée  %d\n", packet_rejete_fifo_pleine_inputQ);
	 		xil_printf("5: Nb de paquets rejetés dans l’interface de sortie %d", packet_rejete_output_port_plein);
	 		xil_printf("6: Nb de paquets maximum dans le fifo d'entrée %d", TaskComputingTCB.MsgQ.NbrEntriesMax);
	 		xil_printf("7: Nb de paquets maximum dans highQ %d", &highQ.MsgQ.NbrEntriesMax );
	 		xil_printf("8: Nb de paquets maximum dans mediumQ %d", &mediumQ.MsgQ.NbrEntriesMax);
	 		xil_printf("9: Nb de paquets maximum dans lowQ %d", &lowQ.MsgQ.NbrEntriesMax);
	 		xil_printf("10: Pourcentage de temps CPU Max de TaskGenerate %d", TaskGenerateTCB.CPUUsageMax);
	 		xil_printf("11: Pourcentage de temps CPU Max TaskComputing %d", TaskComputingTCB.CPUUsageMax);
	 		xil_printf("12: Pourcentage de temps CPU Max TaskFowarding %d", TaskForwardingTCB.CPUUsageMax);
	 		xil_printf("13: Pourcentage de temps CPU Max TaskOutputPort no 1 %d", TaskOutputPortTCB[1].CPUUsageMax);
	 		xil_printf("14: Pourcentage de temps CPU Max TaskOutputPort no 2 %d", TaskOutputPortTCB[2].CPUUsageMax);
	 		xil_printf("15: Pourcentage de temps CPU Max TaskOutputPort no 3 %d", TaskOutputPortTCB[3].CPUUsageMax);
	 		OSMutexPost(&mutPrint, OS_OPT_POST_NONE, &err);

	 		Suspend_Delay_Resume_All(10);

	 		OSTimeDlyHMSM(0, 0, 10, 0, OS_OPT_TIME_HMSM_STRICT, &err);
	 	}

}

/*
 *********************************************************************************************************
 *                                              Suspend_Delay_Resume
 *  -Utilise lors de l'initialisation de la tache statistique
 *  -Permet aussi d'arr�ter l'ex�cution durant l'ex�cution
 *********************************************************************************************************
 */

void Suspend_Delay_Resume_All(int nb_sec) {

OS_ERR err;
int i;

	OSTaskSuspend(&TaskGenerateTCB, &err);
	OSTaskSuspend(&TaskComputingTCB, &err);
	OSTaskSuspend(&TaskForwardingTCB, &err);


	for (i = 0; i < NB_OUTPUT_PORTS; i++) {
		OSTaskSuspend(&TaskOutputPortTCB[i], &err);
	};


	OSTimeDlyHMSM(0, 0, nb_sec, 0, OS_OPT_TIME_HMSM_STRICT, &err);

	OSTaskResume(&TaskGenerateTCB, &err);
	OSTaskResume(&TaskComputingTCB, &err);
	OSTaskResume(&TaskForwardingTCB, &err);

	for (i = 0; i < NB_OUTPUT_PORTS; i++) {
		OSTaskResume(&TaskOutputPortTCB[i], &err);

	}

}

void err_msg(char* entete, uint8_t err)
{
	if(err != 0)
	{
		xil_printf(entete);
		xil_printf(": Une erreur est retourn�e : code %d \n",err);
	}
}



void StartupTask (void *p_arg)
{
	int i;
	OS_ERR err;
    KAL_ERR kal_err;
    CPU_INT32U tick_rate;
#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    MEM_SEG_INFO seg_info;
    LIB_ERR lib_err;
#endif
#if (APP_OSIII_ENABLED == DEF_ENABLED)
#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
    OS_ERR  os_err;
#endif
#endif


    UCOS_IntInit();

#if (APP_OSIII_ENABLED == DEF_ENABLED)
    tick_rate = OS_CFG_TICK_RATE_HZ;
#endif

#if (APP_OSII_ENABLED == DEF_ENABLED)
    tick_rate = OS_TICKS_PER_SEC;
#endif

    UCOS_TmrTickInit(tick_rate);                                /* Configure and enable OS tick interrupt.              */

#if (APP_OSIII_ENABLED == DEF_ENABLED)
#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
    OSStatTaskCPUUsageInit(&os_err);							/* On fait le test du compteur dans la t�che Idle (jalon) */
																/* Notez qu'aucune n'a encore cr��es pour permettre ce test */
#endif
#endif

    KAL_Init(DEF_NULL, &kal_err);
    UCOS_StdInOutInit();
    UCOS_PrintfInit();


#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    UCOS_Print("UCOS - uC/OS Init Started.\r\n");
    UCOS_Print("UCOS - STDIN/STDOUT Device Initialized.\r\n");
#endif

#if (APP_SHELL_ENABLED == DEF_ENABLED)
    UCOS_Shell_Init();
#endif

#if ((APP_FS_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_FS == DEF_ENABLED))
    UCOS_FS_Init();
#endif

#if ((APP_TCPIP_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_NET == DEF_ENABLED))
    UCOS_TCPIP_Init();
#endif /* (APP_TCPIP_ENABLED == DEF_ENABLED) */

#if ((APP_USBD_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_USBD == DEF_ENABLED) && (UCOS_USB_TYPE == UCOS_USB_TYPE_DEVICE))
    UCOS_USBD_Init();
#endif /* #if (APP_USBD_ENABLED == DEF_ENABLED) */

#if ((APP_USBH_ENABLED == DEF_ENABLED) && (UCOS_CFG_INIT_USBH == DEF_ENABLED) && (UCOS_USB_TYPE == UCOS_USB_TYPE_HOST))
    UCOS_USBH_Init();
#endif /* #if (APP_USBH_ENABLED == DEF_ENABLED) */

#if (UCOS_START_DEBUG_TRACE == DEF_ENABLED)
    Mem_SegRemSizeGet(DEF_NULL, 4, &seg_info, &lib_err);

    UCOS_Printf ("UCOS - UCOS init done\r\n");
    UCOS_Printf ("UCOS - Total configured heap size. %d\r\n", seg_info.TotalSize);
    UCOS_Printf ("UCOS - Total used size after init. %d\r\n", seg_info.UsedSize);
#endif

    UCOS_Print("Programme initialise - \r\n");

	/* On met les statistiques � 0 */
	OSStatReset(&err);

	/* On peut maintenant cr�er les t�ches de l'application */

	for(i = 0; i < NB_OUTPUT_PORTS; i++)
	{
		Port[i].id = i;
		switch(i)
		{
			case 0:
				Port[i].name = "Port 0";
				break;
			case 1:
				Port[i].name = "Port 1";
				break;
			case 2:
				Port[i].name = "Port 2";
				break;
			default:
				break;
		};
	}

	// Creation des taches

	OSTaskCreate(&TaskGenerateTCB, 		"TaskGenerate", 	TaskGenerate,	(void*)0, 	TaskGeneratePRIO, 	&TaskGenerateSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskComputingTCB, 	"TaskComputing", 	TaskComputing, 	(void*)0, 	TaskComputingPRIO, 	&TaskComputingSTK[0u], 	TASK_STK_SIZE / 2, TASK_STK_SIZE, 1024, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

	OSTaskCreate(&TaskForwardingTCB,	"TaskForwarding", 	TaskForwarding,	(void*)0, 	TaskForwardingPRIO, &TaskForwardingSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );

// Pour �viter d'avoir 3 fois le m�me code on a un tableau pour lequel chaque entr�e appel TaskOutputPort avec des param�tres diff�rents
	for(i = 0; i < NB_OUTPUT_PORTS; i++){
	OSTaskCreate(&TaskOutputPortTCB[i], "OutputPort", 	TaskOutputPort, &Port[i], TaskOutputPortPRIO, &TaskOutputPortSTK[i][0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*) 0,(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
	};

	OSTaskCreate(&TaskStatsTCB, "TaskStats", TaskStats, (void*)0, TaskStatsPRIO, &TaskStatsSTK[0u], TASK_STK_SIZE / 2, TASK_STK_SIZE, 1, 0, (void*)0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);


/* On peut suspendre StartUp car elle a complete son travail */

    OSTaskSuspend((OS_TCB *)0,&err);

}
